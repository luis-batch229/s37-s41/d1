const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")

// Create course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// Retrieve all courses
router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

/*
	Mini-Activity: 15 mins
	1. Create a route that will retrieve ALL ACTIVE courses (endpoint: "/")
	2. No need for user to login
	3. Create a controller that will return ALL ACTIVE courses
	4. Send your Postman output screenshot in our batch hangouts

*/
// solution: get all active
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// retrieve a specific course
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// export the router object for index.js file
module.exports = router;